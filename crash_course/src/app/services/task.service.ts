import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from "rxjs";
import { Task } from '../task';

const http_options = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private api_url = 'http://localhost:5000/tasks';

  constructor(private http: HttpClient) { }

  get_tasks(): Observable<Task[]> {
    return this.http.get<Task[]>(this.api_url);
  }

  add_task(task: Task): Observable<Task> {
    return this.http.post<Task>(this.api_url, task, http_options);
  }

  delete_task(task: Task): Observable<Task> {
    const url = `${this.api_url}/${task.id}`;
    return this.http.delete<Task>(url);
  }

  update_task_reminder(task: Task): Observable<Task> {
    const url = `${this.api_url}/${task.id}`;
    return this.http.put<Task>(url, task, http_options);
  }
}
