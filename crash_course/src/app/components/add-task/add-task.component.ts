import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UiService } from "../../services/ui.service";
import { Task } from "../../task";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  @Output() on_add_task: EventEmitter<Task> = new EventEmitter<Task>();

  text!: string;
  day!: string;
  reminder: boolean = false;
  show_add_task: boolean = false;
  subscription: Subscription;

  constructor(private ui_service: UiService) {
    this.subscription = ui_service.on_toggle_add_task().subscribe(
      (value) => (this.show_add_task = value)
    );
  }

  ngOnInit(): void {
  }

  on_submit() {
    if (!this.text) {
      alert("Please add a task");
    }

    const new_task: Task = {
      text: this.text,
      day: this.day,
      reminder: this.reminder
    };

    this.on_add_task.emit(new_task);

    this.text = '';
    this.day = '';
    this.reminder = false;
  }
}
